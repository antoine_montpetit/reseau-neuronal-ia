package gui;

public class ZoneShape extends Shape{

    private double radius;
    private int[] color;
    public ZoneShape(double relX, double relY, double radius, double score){
        super(relX,relY);
        this.radius = radius;

        double maxScore=1;
        if(score>0){
            color = new int[]{(int)(255*(1-score/maxScore)), 255, (int)(255*(1-score/maxScore))};
        }
        else{
            color = new int[]{255, (int)(255*(1+score/maxScore)), (int)(255*(1+score/maxScore))};
        }

    }


    public int[] getColor() {
        return color;
    }

    public double getRadius() {
        return radius;
    }
}
