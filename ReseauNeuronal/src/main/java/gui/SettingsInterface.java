package gui;

public interface SettingsInterface {
    // settings fetched on simulation generation start
    int getAgentAmount();
    double getMutationLevel();
    double getTimePerGen();
    double getGenSpeed();

}
