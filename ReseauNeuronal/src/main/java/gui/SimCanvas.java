package gui;

import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;

public class SimCanvas implements CanvasInterface {

    private Canvas canvasNode;
    private GraphicsContext gc;

    public Canvas getCanvasNode() {
        return canvasNode;
    }

    public SimCanvas(Canvas canvasNode){
        this.canvasNode=canvasNode;
        this.gc=canvasNode.getGraphicsContext2D();
    }

    @Override
    public void drawCircle(double x, double y, double rad, int[] color) {
        gc.setFill(Color.color(((double)color[0])/255,((double)color[1])/255,((double)color[2])/255));
        gc.fillOval(x-rad/2,y-rad/2,rad,rad);
    }

    @Override
    public void drawRelativeCircle(double relX, double relY, double relRad, int[] color) {
        double x = relX * canvasNode.getWidth();
        double y = relY * canvasNode.getHeight();
        double rad = relRad * canvasNode.getWidth();
        drawCircle(x,y,rad,color);
    }

    @Override
    public void drawGenNumber(int gen) {
        gc.setTextAlign(TextAlignment.LEFT);
        gc.setTextBaseline(VPos.TOP);
        gc.setFont(Font.font("Verdana", FontWeight.BOLD,canvasNode.getWidth()/60));

        String text = "Génération #"+gen;
        gc.setFill(Color.color(0,0,0));
        gc.fillText(text, canvasNode.getWidth()/80,canvasNode.getHeight()/80);
        gc.setStroke(Color.color(1,1,1));
        gc.strokeText(text,canvasNode.getWidth()/80,canvasNode.getHeight()/80);
    }

    @Override
    public void drawBackground(int[] color) {
        gc.setFill(Color.color(((double)color[0])/255,((double)color[1])/255,((double)color[2])/255));
        gc.fillRect(0,0,canvasNode.getWidth(),canvasNode.getHeight());
        gc.fill();
    }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public double getHeight() {
        return 0;
    }


    // canvas testing
        /*final Canvas canvas = new Canvas(300,300);
        Group root = new Group();
        GraphicsContext gc= canvas.getGraphicsContext2D();
        gc.setLineWidth(150);
        final double steps=4096;
        for(double i=0;i<steps;i++){
            //gc.setStroke(Color.hsb(((double)i)/steps*360,1,1));
            gc.setStroke(Color.hsb(((double)i)/steps*360,(sin(((double)i)/steps*8*PI)+1)/2,1));
            gc.beginPath();
            gc.moveTo(150+75*cos(((double)i)/steps*2*PI),150+75*sin(((double)i)/steps*2*PI));
            gc.lineTo(150+75*cos((((double)i)+1)/steps*2*PI),150+75*sin((((double)i)+1)/steps*2*PI));
            gc.closePath();
            //gc.strokeLine(125+100*cos(i/30*2*PI),125+100*sin(i/30*2*PI),125+100*cos((i+1)/30*2*PI),125+100*sin((i+1)/30*2*PI));
            gc.stroke();

        }
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root,300,300, Color.GRAY));
        primaryStage.show();*/
}
