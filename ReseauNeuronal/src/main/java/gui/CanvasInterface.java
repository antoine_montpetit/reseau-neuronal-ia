package gui;

import javafx.scene.canvas.Canvas;

public interface CanvasInterface {
    void drawCircle(double x, double y, double rad, int[] color);
    void drawRelativeCircle(double relX, double relY, double relRad, int[] color);
    void drawGenNumber(int gen);
    void drawBackground(int[] color);
    double getWidth();
    double getHeight();
    Canvas getCanvasNode();
}
