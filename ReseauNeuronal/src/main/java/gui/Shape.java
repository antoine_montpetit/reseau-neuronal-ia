package gui;

public abstract class Shape {
    private double relX;
    private double relY;


    Shape(double relX, double relY){
        this.relX = relX;
        this.relY = relY;
    }

    public abstract int[]getColor();

    public double getRelX() {
        return relX;
    }

    public double getRelY() {
        return relY;
    }

}


