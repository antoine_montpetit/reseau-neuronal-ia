package gui;

import java.util.ArrayList;

public class SimShape {

    private ArrayList<AgentShape> agentShapes;
    private ArrayList<ZoneShape> zoneShapes;
    private int genNumber;

   public SimShape(ArrayList<AgentShape> agentShapes, ArrayList<ZoneShape> zoneShapes, int genNumber){
       this.agentShapes = agentShapes;
       this.zoneShapes = zoneShapes;
       this.genNumber = genNumber;
   }


    public ArrayList<AgentShape> getAgentShapes() {
        return agentShapes;
    }

    public ArrayList<ZoneShape> getZoneShapes() {
        return zoneShapes;
    }

    public int getGenNumber() {
        return genNumber;
    }
}
