package gui;

import ai.AIInterface;
import controllers.BasicController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import mikera.vectorz.Vector;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author Antoine Montpetit
 */
public class AIGUI {

    // Manage the layout
    public static final double BORDER_OFFSET = 20.0;
    public static final int SCALE_FACTOR = 2;

    // Changes the width of the line for the weight
    private static final double LINE_WIDTH = 5;

    // Name Spacing
    public static final double NAME_SPACING = 50.0;

    // Color parameters
    public static final Color POSITIVE_COLOR = Color.color(0,0,1);
    public static final Color NEGATIVE_COLOR = Color.color(1,0,0);
    private static final Paint NODE_POINT_COLOR = Color.color(0.75,0.75,0.75);

    // parameters
    private BasicController basicController;

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    private AnchorPane anchorPane;

    public AIInterface getCurrAI() {
        return currAI;
    }

    private AIInterface currAI;
    private double width;
    private double height;

    // constructor
    public AIGUI(BasicController basicController, double width, double height){
        this.basicController = basicController;

        anchorPane = new AnchorPane();
        setHeight(height);
        setWidth(width);

        //anchorPane.setPadding(new Insets(BORDER_OFFSET));

        if(basicController.getContainerVBox().getChildren().size()==0)
            basicController.getContainerVBox().getChildren().add(anchorPane);
        else
            basicController.getContainerVBox().getChildren().set(0,anchorPane);
    }

    // Change the showed AI
    public void updateAI(AIInterface newAI){
        currAI = newAI;
        updateAI();
        /*ArrayList<Vector> nodesValue = newAI.getNodes();
        ArrayList<ArrayList<Vector>> weightValues = newAI.getLinks();

        anchorPane.getChildren().clear();

        showLinks(nodesValue, weightValues);
        showNodes(nodesValue);
        anchorPane.getChildren().add(new Line(0,height/2+NAME_SPACING/2,width,height/2+NAME_SPACING/2));
        anchorPane.getChildren().add(new Line(0,BORDER_OFFSET+NAME_SPACING,width,BORDER_OFFSET+NAME_SPACING));
        anchorPane.getChildren().add(new Line(0,height-BORDER_OFFSET,width,height-BORDER_OFFSET));
        anchorPane.getChildren().add(new Line(width/2,0,width/2,height));
        //setWidth(width);
        //setHeight(height);
        setWidth(900);
        System.out.println(anchorPane.getPrefWidth()+", "+width);
        showName(newAI);*/
    }

    public void updateAI(){
        ArrayList<Vector> nodesValue = currAI.getNodes();
        ArrayList<ArrayList<Vector>> weightValues = currAI.getLinks();

        anchorPane.getChildren().clear();

        showLinks(nodesValue, weightValues);
        showNodes(nodesValue);
        /*anchorPane.getChildren().add(new Line(0,height/2+NAME_SPACING/2,width,height/2+NAME_SPACING/2));
        anchorPane.getChildren().add(new Line(0,BORDER_OFFSET+NAME_SPACING,width,BORDER_OFFSET+NAME_SPACING));
        anchorPane.getChildren().add(new Line(0,height-BORDER_OFFSET,width,height-BORDER_OFFSET));
        anchorPane.getChildren().add(new Line(width/2,0,width/2,height));*/
        //setWidth(width);
        //setHeight(height);
        /*setWidth(900);
        System.out.println(anchorPane.getPrefWidth()+", "+width);*/
        showName(currAI);
    }

    // Returns the width
    public double getWidth() {
        return width;
    }

    // Set the width
    public void setWidth(double width) {
        this.width = width;
        anchorPane.setMinWidth(width);
        anchorPane.setPrefWidth(width);
        anchorPane.setMaxWidth(width);
    }

    // Returns the height
    public double getHeight() {
        return height;
    }

    // Set the height
    public void setHeight(double height) {
        this.height = height;
        anchorPane.setMinHeight(height);
        anchorPane.setPrefHeight(height);
        anchorPane.setMaxHeight(height);
    }

    // Get the position of a given node in the network
    private double[] getNodePosition(int layer, int nodeID, int currLayerLength, double[] layoutData){
        double x = ((layer)*(width-layoutData[0]-2*BORDER_OFFSET)/(layoutData[3]-1))+BORDER_OFFSET;//(layer * layoutData[1]) /*- (layoutData[0]/2.0)*/ + BORDER_OFFSET;
        double realHeight=((height-2*BORDER_OFFSET-NAME_SPACING));
        double innerOffset=(realHeight-((currLayerLength-1)*SCALE_FACTOR*layoutData[0]+2*layoutData[0]))/2;
        //System.out.println(layoutData[0]*currLayerLength+"");
        //System.out.println(innerOffset/realHeight);
        double y = ((nodeID-1)*SCALE_FACTOR*layoutData[0]+2*layoutData[0])+layoutData[0]/2+innerOffset+BORDER_OFFSET+NAME_SPACING;//((nodeID - (currLayerLength/2.0)) * layoutData[2]) + ((height-BORDER_OFFSET*2-NAME_SPACING)/2.0)  + BORDER_OFFSET + NAME_SPACING;
        //System.out.println(y);
        return new double[]{x, y};
    }

    // Get basic information on the scale for the given width or height
    private double[] getLayoutData(int numberOfLayer, int maxLayerLength){
        //System.out.println(anchorPane.getHeight()+", "+this.height);
        //System.out.println(maxLayerLength);
        double deltaY = (height-(BORDER_OFFSET*2)-NAME_SPACING)/(((maxLayerLength-1) * SCALE_FACTOR)+1);
        double deltaX = (width-(BORDER_OFFSET*2))/((numberOfLayer-1)*SCALE_FACTOR);

        double spacingX = deltaX *SCALE_FACTOR;
        double spacingY = deltaY * SCALE_FACTOR;

        double diameter = Math.min(deltaX, deltaY);

        //System.out.println(diameter*maxLayerLength*SCALE_FACTOR-diameter+2*BORDER_OFFSET+NAME_SPACING);

        return new double[]{diameter, spacingX, spacingY, numberOfLayer};
    }

    // Gets the biggest layer length
    private int getMaxLayerLength(ArrayList<Vector> nodesStructure){
        // Get the biggest layer size
        int maxLayerLength = 0;
        for (Vector currVector:nodesStructure) {
            if(currVector.length() > maxLayerLength)
                maxLayerLength = currVector.length();
        }
        return maxLayerLength;
    }

    // Show the nodes
    private void showNodes(ArrayList<Vector> nodesValues){

        double[] layoutData = getLayoutData(nodesValues.size(), getMaxLayerLength(nodesValues));

        for(int i = 0; i < nodesValues.size(); i++){

            Vector currLayer = nodesValues.get(i);

            for (int j = 0; j < currLayer.length(); j++){

                double[] pos = getNodePosition(i, j, currLayer.length(), layoutData);

                Circle circle = new Circle(layoutData[0]/2.0);
                circle.setFill(NODE_POINT_COLOR);

                Label text = new Label(Math.round(currLayer.get(j)*100)/100.0 + "");
                text.setMinWidth(layoutData[0]*2);
                text.setMinHeight(layoutData[0]*2);
                text.setAlignment(Pos.CENTER);
                text.setFont(Font.font("Verdana",FontWeight.BOLD, layoutData[0]/2));

                Tooltip tooltip = new Tooltip("Value: " + currLayer.get(j));
                tooltip.setFont(Font.font("Verdana",FontWeight.BOLD, 12));
                Tooltip.install(circle, tooltip);
                Tooltip.install(text, tooltip);

                AnchorPane.setTopAnchor(circle, pos[1]);
                AnchorPane.setLeftAnchor(circle, pos[0]);
                AnchorPane.setTopAnchor(text, pos[1]-layoutData[0]+layoutData[0]/2);
                AnchorPane.setLeftAnchor(text, pos[0]-layoutData[0]+layoutData[0]/2);
                //anchorPane.getChildren().add(new Circle(pos[0]-50,pos[1]-50,2));
                //anchorPane.getChildren().add(new Circle(pos[0]+50,pos[1]+50,2));


                anchorPane.getChildren().addAll(circle,text);

            }
            Circle dummy = new Circle(0);
            AnchorPane.setTopAnchor(dummy, height+layoutData[0]);
            AnchorPane.setLeftAnchor(dummy, width+layoutData[0]);
            anchorPane.getChildren().add(dummy);

        }

    }

    // Show the links
    private void showLinks(ArrayList<Vector> nodesValues, ArrayList<ArrayList<Vector>> weightValues) {

        double[] layoutData = getLayoutData(nodesValues.size(), getMaxLayerLength(nodesValues));
        double radius = layoutData[0]/2.0;

        for (int i = 1; i < nodesValues.size(); i++){
            for (int n = 0; n < nodesValues.get(i).length(); n++){
                for (int m = 0; m < nodesValues.get(i-1).length(); m++){

                    double[] pos1 = getNodePosition(i-1,m,nodesValues.get(i-1).length(),layoutData);
                    double[] pos2 = getNodePosition(i,n,nodesValues.get(i).length(),layoutData);

                    double valueOfLink = weightValues.get(i - 1).get(n).get(m);

                    Color colorType = valueOfLink > 0 ? POSITIVE_COLOR : NEGATIVE_COLOR;
                    double alpha = Math.abs(valueOfLink);
                    Color color = Color.color(colorType.getRed(), colorType.getGreen(), colorType.getBlue(), alpha);

                    Line currLine = new Line(pos1[0] + radius, pos1[1] + radius, pos2[0] + radius, pos2[1] + radius);
                    currLine.setStroke(color);
                    currLine.setStrokeWidth(LINE_WIDTH*alpha);

                    anchorPane.getChildren().add(currLine);

                }
            }

        }

    }

    // Show the names
    private void showName(AIInterface newAI) {

        Label nameLabel = new Label("Name: " + newAI.getName());
        Label idLabel = new Label("ID: " + newAI.getID());

        AnchorPane.setLeftAnchor(nameLabel,BORDER_OFFSET);
        AnchorPane.setTopAnchor(nameLabel, BORDER_OFFSET);

        AnchorPane.setRightAnchor(idLabel, BORDER_OFFSET);
        AnchorPane.setTopAnchor(idLabel, BORDER_OFFSET);

        anchorPane.getChildren().addAll(nameLabel, idLabel);

    }

}
