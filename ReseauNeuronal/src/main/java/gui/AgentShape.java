package gui;

public class AgentShape extends Shape{

    private double relSize;
    private int[]color;

    public AgentShape(double relX, double relY, double relSize, int[] color) {
        super(relX, relY);

        this.relSize = relSize;
        this.color = color;

    }

    public int[] getColor() {
        return color;
    }


    public double getRelSize() {
        return relSize;
    }
}
