package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class BasicController {

    public HBox getContainerHBox() {
        return containerHBox;
    }

    public void setContainerHBox(HBox containerHBox) {
        this.containerHBox = containerHBox;
    }

    public VBox getContainerVBox() {
        return containerVBox;
    }

    public void setContainerVBox(VBox containerVBox) {
        this.containerVBox = containerVBox;
    }

    @FXML
    private HBox containerHBox;

    @FXML
    private VBox containerVBox;

    @FXML
    void loadButton(ActionEvent event) {

    }

    @FXML
    void saveButton(ActionEvent event) {

    }

}
