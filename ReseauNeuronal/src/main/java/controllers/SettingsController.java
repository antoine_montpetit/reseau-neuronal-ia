package controllers;

import gui.SettingsInterface;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import simulation.SimulationInterface;

public class SettingsController implements SettingsInterface {
    public final double DUREE_GENERATION_MAX = 5000;
    public final double VITESSE_SIMULATION_MAX = 200;
    public final int NOMBRE_AGENT_MAX = 200;
    public final double DUREE_GENERATION_MIN = 10;
    public final double VITESSE_SIMULATION_MIN = 1;
    public final int NOMBRE_AGENT_MIN = 1;

    public final double DUREE_GENERATION_DEFAULT = 50;
    public final double VITESSE_SIMULATION_DEFAULT = 50;
    public final int NOMBRE_AGENT_DEFAULT = 100;




    private boolean isPlaying;

    private SimulationInterface simulationInterface;
    private SettingsInterface settingsInterface;

    public void setSimulationInterface(SimulationInterface simulationInterface) {
        this.simulationInterface = simulationInterface;
    }

    public void setSettingsInterface(SettingsInterface settingsInterface) {
        this.settingsInterface = settingsInterface;
    }

    @FXML
    private void initialize(){
        isPlaying = false;
        boutonPlay.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("imageSettings/PauseUI.png"))));
        boutonPlay.setPadding(Insets.EMPTY);
        boutonReset.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("imageSettings/ResetUI.png"))));
        boutonReset.setPadding(Insets.EMPTY);

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                niveauMutation.setText(String.format("%.2f",newValue));
            }
        });
        currentVitesse.setText(String.valueOf(VITESSE_SIMULATION_DEFAULT));
        currentDuree.setText(String.valueOf(DUREE_GENERATION_DEFAULT));
        currentNombreAgent.setText(String.valueOf(NOMBRE_AGENT_DEFAULT));

    }
    @FXML
    private Label currentDuree;

    @FXML
    private Label currentVitesse;

    @FXML
    private Label currentNombreAgent;

    @FXML
    private Button boutonPlay;

    @FXML
    private Button boutonReset;

    @FXML
    private TextField dureeGeneration;

    @FXML
    private TextField vitesseSimulation;

    @FXML
    private TextField nombreAgentTextField;

    @FXML
    private Slider slider;

    @FXML
    private Label niveauMutation;

    @FXML
    private CheckBox infiniteCheckbox;


    public boolean getIsInfiniteTime()
    {
        System.out.println("Infinite Time was gotten got thingy thing got got gotten got in the thingy gotten got gotten");
        return infiniteCheckbox.isSelected();

    }


    @FXML
    void updateBoutonPlay(ActionEvent event) {
        isPlaying = !isPlaying;
        if (isPlaying){
            boutonPlay.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("imageSettings/PlayUI.png"))));
        }
        else{
            boutonPlay.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("imageSettings/PauseUI.png"))));
        }
        simulationInterface.togglePause();
    }

    @FXML
    void updateBoutonReset(ActionEvent event) {
        simulationInterface.reset();
    }

    @FXML
    void updateDuree(ActionEvent event) {
            if(validateDureeGeneration())
            {
                this.currentDuree.setText(dureeGeneration.getText());
            }
            dureeGeneration.setText("");

    }

    @FXML
    void updateNombreAgent(ActionEvent event) {
            if(validateNombreAgent())
            {
                this.currentNombreAgent.setText(nombreAgentTextField.getText());
            }
            nombreAgentTextField.setText("");
    }

    @FXML
    void updateVitesse(ActionEvent event) {
            if(validateVitesseGeneration())
            {
                this.currentVitesse.setText(vitesseSimulation.getText());
                simulationInterface.updateSimulationSpeed(Integer.parseInt(currentVitesse.getText()));
            }
            vitesseSimulation.setText("");
    }
    
        // BONNE VERSION DU CODE , PEUT ETRE RAJOUTER RECEVOIR PARAMETRE UN DOUBLE YA YA YA
    private boolean validateDureeGeneration(){
        boolean temp = true;
        try {
            if (Double.parseDouble(dureeGeneration.getText()) > DUREE_GENERATION_MAX || Double.parseDouble(dureeGeneration.getText()) < DUREE_GENERATION_MIN)
            {
                temp = false;
            }
        }
        catch(Exception e)
        {
                temp = false;
                System.err.println(e);
        }
        finally {
            if (temp == false)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Veuillez entrer une valeur valide entre " + DUREE_GENERATION_MIN + " et " + DUREE_GENERATION_MAX);
                alert.setHeaderText("Durée de génération invalide");
                alert.show();
            }
        }
        return temp;
    }

    private boolean validateVitesseGeneration(){
        boolean temp = true;
        try {
            if (Double.parseDouble(vitesseSimulation.getText()) > VITESSE_SIMULATION_MAX || Double.parseDouble(vitesseSimulation.getText()) < VITESSE_SIMULATION_MIN)
            {
                temp = false;
            }

        }
        catch(NullPointerException | NumberFormatException e)
        {
           temp = false;
           System.err.println(e);
        }
        finally {
            if(temp == false)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Veuillez entrer une valeur valide entre " + VITESSE_SIMULATION_MIN + " et " + VITESSE_SIMULATION_MAX);
                alert.setHeaderText("Vitesse de génération invalide");
                alert.show();
            }
        }
        return temp;
    }

    private boolean validateNombreAgent(){
        boolean temp = true;
        try {
            if(Integer.parseInt(nombreAgentTextField.getText())< NOMBRE_AGENT_MIN ||Integer.parseInt(nombreAgentTextField.getText())> NOMBRE_AGENT_MAX)
            {
                temp = false;
            }
        }
        catch(NumberFormatException e)
        {
            temp = false;
            System.err.println(e);
        }
        finally
        {
            if(temp == false)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Veuillez entrer une valeur entre " + NOMBRE_AGENT_MIN + " et " + NOMBRE_AGENT_MAX);
                alert.setHeaderText("Nombre d'agent invalide");
                alert.show();
            }
        }
        return temp;
    }


    @Override
    public int getAgentAmount() {
        return Integer.parseInt(currentNombreAgent.getText());
    }

    @Override
    public double getMutationLevel() {
        return Double.parseDouble(niveauMutation.getText().replace(",","."));
    }

    @Override
    public double getTimePerGen() {
        return Double.parseDouble(currentDuree.getText());
    }

    @Override
    public double getGenSpeed() {
        return Double.parseDouble(currentVitesse.getText());
    }
}
