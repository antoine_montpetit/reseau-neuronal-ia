package controllers;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;

public class CanvasController {

    @FXML
    private Canvas mainCanvas;

    public Canvas getMainCanvas() {
        return mainCanvas;
    }
}
