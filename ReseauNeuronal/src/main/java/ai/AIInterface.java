package ai;

import mikera.vectorz.Vector;

import java.util.ArrayList;

public interface AIInterface {
    String getName();
    void setName(String name);
    long getID();
    Vector compute(Vector inputs);
    ArrayList<ArrayList<Vector>> getLinks();
    ArrayList<Vector> getNodes();
    void setScore(double score);
    double getScore();
    AIInterface mutate();
}
