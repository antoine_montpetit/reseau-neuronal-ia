package ai;

import mikera.vectorz.AVector;
import mikera.vectorz.Vector;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class AIStub implements AIInterface{

    private ArrayList<Vector> nodes;
    private ArrayList<ArrayList<Vector>> links;

    public AIStub(){

        // Create Random Nodes
        nodes = new ArrayList<Vector>();
        /*int LayerNumber = (int) (3);
        for (int i = 0; i < LayerNumber; i++){

            int NodeNumber = (int) (7);
            Vector vector = Vector.createLength(NodeNumber);

            for (int j = 0; j < NodeNumber; j ++){
                vector.set(j,(Math.random()*2)-1);
            }

            nodes.add(vector);
        }*/
        int LayerNumber = (int) ((Math.random() * (10-2))+2);
        for (int i = 0; i < LayerNumber; i++){

            int NodeNumber = (int) ((Math.random() * (20-1))+1);
            Vector vector = Vector.createLength(NodeNumber);

            for (int j = 0; j < NodeNumber; j ++){
                vector.set(j,(Math.random()*2)-1);
            }

            nodes.add(vector);
        }

        // Create Random Weight
        links = new ArrayList<ArrayList<Vector>>();

        for (int i = 1; i < nodes.size(); i++) {
            ArrayList<Vector> weights = new ArrayList<Vector>();
            for (int n = 0; n < nodes.get(i).length(); n++) {
                Vector weight = Vector.createLength(nodes.get(i - 1).length());
                for (int m = 0; m < nodes.get(i - 1).length(); m++) {
                    weight.set(m,(Math.random()*2)-1);
                }
                weights.add(weight);
            }
            links.add(weights);
        }



    }

    @Override
    public String getName() {
        return "This is a preview";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public long getID() {
        return (long)(Math.random()*Double.MAX_VALUE);
    }

    @Override
    public Vector compute(Vector inputs) {
        return null;
    }

    @Override
    public ArrayList<ArrayList<Vector>> getLinks() {
        return links;
    }

    @Override
    public ArrayList<Vector> getNodes() {
        return nodes;
    }

    @Override
    public void setScore(double score) {

    }

    @Override
    public double getScore() {
        return 0;
    }

    @Override
    public AIInterface mutate() {
        return null;
    }
}
