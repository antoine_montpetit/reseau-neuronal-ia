package ai;

import mikera.vectorz.Vector;
import simulation.AIManager;

import java.util.ArrayList;
import java.util.Random;

public class AI implements AIInterface {
    public static final int MIN_LAYERS = 3;
    public static final int MAX_LAYERS = 8;
    public static final int INPUT_NODES = 6;
    public static final int OUTPUT_NODES = 2;
    public static final int MIN_NODES = 2;
    public static final int MAX_NODES = 10;

    public static final double BASE_MUTATION_LEVEL = 0.05;

    private String name;
    private long id;
    private ArrayList<Vector> nodes;
    private ArrayList<ArrayList<Vector>> links;
    private double score;
    private AIManager aiManager;

    public AI(AIManager manager){
        Random random = new Random();
        generateNodes();
        generateLinks();
        id = Math.abs(random.nextLong());
        name=id+"";
        score=0.0;
        aiManager=manager;
    }

    public AI(String name,AIManager manager){
        this(manager);
        this.name = name;
    }

    public AI(ArrayList<Vector> nodes,ArrayList<ArrayList<Vector>> links,AIManager manager){
        this(manager);
        this.nodes=nodes;
        this.links=links;
    }

    public AI(ArrayList<Vector> nodes,ArrayList<ArrayList<Vector>> links,String name,AIManager manager){
        this(nodes,links,manager);
        this.name = name;
    }


    private void generateNodes(){
        Random random = new Random();
        int layerAmt = (int)(Math.abs(random.nextGaussian())*(MAX_LAYERS-MIN_LAYERS)+MIN_LAYERS);
        nodes=new ArrayList<Vector>();
        for(int i=0;i<layerAmt;i++){
            // overrides for first and last layer
            int layerHeight = (i==0)?INPUT_NODES:(i==layerAmt-1)?OUTPUT_NODES:(int)(Math.abs(random.nextGaussian())*(MAX_NODES-MIN_NODES)+MIN_NODES);

            ArrayList<Double> currLayer = new ArrayList<Double>();

            for(int j=0;j<layerHeight;j++){
                currLayer.add(random.nextDouble()*2-1);
            }

            nodes.add(Vector.create(currLayer));
        }
    }

    private void generateLinks(){
        // generate nodes first, otherwise this will fail catastrophically
        links=new ArrayList<ArrayList<Vector>>();
        // generate the links backwards from last to first layer
        // for all layers except the first
        for(int i=1;i<nodes.size();i++){
            // create new blank array for vectors
            ArrayList<Vector> currLayerNodes = new ArrayList<Vector>();
            // for all nodes in the current layer
            for(int j=0;j<nodes.get(i).length();j++){
                // create an array for values
                ArrayList<Double> currNodeLinks = new ArrayList<Double>();
                // for all nodes in the previous layer
                for(int k=0;k<nodes.get(i-1).length();k++){
                    // add a random weight to the current list
                    currNodeLinks.add(Math.random()*2-1);
                }
                currLayerNodes.add(Vector.create(currNodeLinks));
            }
            links.add(currLayerNodes);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name=name;
    }

    @Override
    public long getID() {
        return id;
    }

    @Override
    public Vector compute(Vector inputs) {
        // replace first nodes with actual inputs
        nodes.set(0,inputs);
        // for all layers except outputs
        for(int i=0;i<nodes.size()-1;i++){
            // create a temp list for the next layer's new node values
            ArrayList<Double> nextNodes = new ArrayList<Double>();
            // add the dot products of the node layer and the appropriate links to the node values list
            for(int j=0;j<nodes.get(i+1).length();j++){
                // not divided by the number of nodes in the previous layer
                nextNodes.add(j,nodes.get(i).dotProduct(links.get(i).get(j))/*/nodes.get(i).length()*/);
            }
            nodes.set(i+1,Vector.create(nextNodes));
        }

        return this.nodes.get(this.nodes.size()-1);
    }

    @Override
    public ArrayList<ArrayList<Vector>> getLinks() {
        return links;
    }

    @Override
    public ArrayList<Vector> getNodes() {
        return nodes;
    }

    @Override
    public void setScore(double score) {
        this.score=score;
    }

    @Override
    public double getScore() {
        return score;
    }

    @Override
    public AIInterface mutate() {
        ArrayList<Vector> newNodes = (ArrayList<Vector>)nodes.clone();
        ArrayList<ArrayList<Vector>> newLinks = (ArrayList<ArrayList<Vector>>)links.clone();

        // SHAKE BOX :P
        // [[~]] (shaking the box)

        for(int i=0;i<newLinks.size();i++){
            for(int j=0;j<newLinks.get(i).size();j++){
                for(int k=0;k<newLinks.get(i).get(j).length();k++){
                    double val=newLinks.get(i).get(j).get(k)+(Math.random()*2-1)*((aiManager==null)?1:(aiManager.getSettings().getMutationLevel()))*BASE_MUTATION_LEVEL;
                    val=Math.max(-1, Math.min(1,val));
                    newLinks.get(i).get(j).set(k,val);
                }
            }
        }

        return new AI(newNodes,newLinks,"Mutated",null);
    }

    @Override
    public String toString() {
        return "AI{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", nodes=" + nodes +
                ", links=" + links +
                ", score=" + score +
                '}';
    }

    public static void main(String[] args) {
        AI ai = new AI(null);
        //AI ai2 = new AI("hello");

        System.out.println(ai+"\n");

        ai.compute(Vector.create(new double[]{0.5,0.1,0.7,0.4,0.3,0.2}));

        System.out.println(ai);

        /*ArrayList<Vector> testNodes = new ArrayList<Vector>();
        testNodes.add(Vector.create(new double[]{0.0,2.6,10.6,-234,9812}));
        ArrayList<ArrayList<Vector>> testLinks = new ArrayList<ArrayList<Vector>>();
        ArrayList<Vector> temp = new ArrayList<Vector>();
        temp.add(Vector.create(new double[]{942,9.4,28,-238,1}));
        testLinks.add(temp);

        AI ai3 = new AI(testNodes,testLinks);
        AI ai4 = new AI(testNodes,testLinks,"wasd");

        System.out.println(ai+"\n\n");
        System.out.println(ai2+"\n\n");
        System.out.println(ai3+"\n\n");
        System.out.println(ai4);*/
    }
}
