package app;

import ai.AI;
import ai.AIInterface;
import ai.AIStub;
import controllers.BasicController;
import controllers.SettingsController;
import gui.*;
import controllers.CanvasController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import mikera.vectorz.Vector;

import simulation.*;


import static java.lang.Math.*;

import javafx.stage.Stage;


public class App extends Application {

    BasicController controller;
    AIGUI aigui;
    AIInterface currAI;
    SimulationService simulationService;

    public BasicController getController() {
        return controller;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // load basic interface
        FXMLLoader baseLoader = new FXMLLoader(this.getClass().getResource("basicInterface.fxml"));
        Parent node = baseLoader.load();
        controller = baseLoader.getController();
        primaryStage.setScene(new Scene(node));
        primaryStage.setMaximized(true);

        primaryStage.show();

        // window size override
        primaryStage.setMinWidth(primaryStage.getWidth());
        primaryStage.setMinHeight(primaryStage.getHeight());

        /*HBox.setHgrow(controller.getContainerVBox(),Priority.ALWAYS);
        controller.getContainerVBox().setPrefWidth(Double.MAX_VALUE);
        controller.getContainerVBox().setMaxWidth(Double.MAX_VALUE);*/
        //controller.getContainerVBox().setBackground(new Background(new BackgroundFill(Color.color(1,0,0),CornerRadii.EMPTY, Insets.EMPTY)));


        /*
        // canvas testing
        final Canvas canvas = new Canvas(300,300);
        Group root = new Group();
        GraphicsContext gc= canvas.getGraphicsContext2D();
        gc.setLineWidth(150);
        final double steps=4096;
        for(double i=0;i<steps;i++){
            //gc.setStroke(Color.hsb(((double)i)/steps*360,1,1));
            gc.setStroke(Color.hsb(((double)i)/steps*360,(sin(((double)i)/steps*8*PI)+1)/2,1));
            gc.beginPath();
            gc.moveTo(150+75*cos(((double)i)/steps*2*PI),150+75*sin(((double)i)/steps*2*PI));
            gc.lineTo(150+75*cos((((double)i)+1)/steps*2*PI),150+75*sin((((double)i)+1)/steps*2*PI));
            gc.closePath();
            //gc.strokeLine(125+100*cos(i/30*2*PI),125+100*sin(i/30*2*PI),125+100*cos((i+1)/30*2*PI),125+100*sin((i+1)/30*2*PI));
            gc.stroke();
         */

        // load canvas component and add a request focus listener
        FXMLLoader canvasLoader = new FXMLLoader(this.getClass().getResource("mainCanvas.fxml"));
        Parent canvasParent = canvasLoader.load();

        ((CanvasController)canvasLoader.getController()).getMainCanvas().setOnMouseClicked((e)->{
            ((CanvasController)canvasLoader.getController()).getMainCanvas().requestFocus();
        });

        // look at clicked agent
        ((CanvasController)canvasLoader.getController()).getMainCanvas().setOnMouseClicked((e)->{
            double threshold = 2;
            for(AgentInterface agent:simulationService.getSimulation().getAgentManager().getAgents()){
                if(Math.pow(e.getX()-agent.getRelX()*1000,2)+Math.pow(e.getY()-agent.getRelY()*1000,2)<Math.pow(agent.getRelSize()*1000,2)*threshold){
                    currAI = agent.getAI();
                    aigui.updateAI(agent.getAI());
                    break;
                }
            };
        });




        //((CanvasController)canvasLoader.getController()).getMainCanvas().setFocusTraversable(true);

        controller.getContainerHBox().getChildren().add(0, canvasParent);
        CanvasInterface simCanvas = new SimCanvas(((CanvasController)canvasLoader.getController()).getMainCanvas());
        // add debug listeners to canvas
        /*simCanvas.drawBackground(new int[]{120,120,120});
        simCanvas.getCanvasNode().setOnScroll((e)->{
                //System.out.println("circles");
                simCanvas.drawBackground(new int[]{120,120,120});
                for(int i=0;i<3000;i++) {
                    simCanvas.drawRelativeCircle(random()*1, random()*1, random()*0.01, new int[]{(int)(random()*255), (int)(random()*255), (int)(random()*255)});
                }
                simCanvas.drawGenNumber((int)(random()*10000));

        });*/

        // Load AI GUI
        //System.out.println(primaryStage.getWidth()-((Canvas)((BorderPane)canvasParent).getChildren().get(0)).getWidth());
        //AI ai2 = new AI("hello");
        AI ai = new AI(null);
        ai.compute(Vector.create(new double[]{0.5,0.1,0.7,0.4,0.3,0.2}));
        System.out.println(ai+"\n");

        aigui = new AIGUI(controller, /*primaryStage.getWidth()-((Canvas)((BorderPane)canvasParent).getChildren().get(0)).getWidth()*/920, 600);
        aigui.updateAI(ai);
        aigui.getAnchorPane().setOnScroll((e)->{
            /*AI newAi = new AI();
            newAi.compute(Vector.create(new double[]{0.5,0.1,0.7,0.4,0.3,0.2}));
            System.out.println(newAi+"\n");
            aigui.updateAI(newAi);*/
            aigui.updateAI(aigui.getCurrAI().mutate());
            aigui.getCurrAI().compute(Vector.create(new double[]{0.5,0.1,0.7,0.4,0.3,0.2}));
            //System.out.println(aigui.getCurrAI()+"\n");
        });
        aigui.getAnchorPane().setOnMouseClicked((e)->{
            AI newAi = new AI(null);
            newAi.compute(Vector.create(new double[]{0.5,0.1,0.7,0.4,0.3,0.2}));
            System.out.println(newAi+"\n");
            aigui.updateAI(newAi);
            /*aigui.updateAI(aigui.getCurrAI().mutate());
            aigui.getCurrAI().compute(Vector.create(new double[]{0.5,0.1,0.7,0.4,0.3,0.2}));
            System.out.println(aigui.getCurrAI()+"\n");*/
        });
        //aigui.getAnchorPane().setBackground(new Background(new BackgroundFill(Color.color(1,0,0),CornerRadii.EMPTY, Insets.EMPTY)));

        // add debug for test agent
        setKeyMouvement(primaryStage.getScene(), simCanvas, new Agent());

        // load settings panel
        FXMLLoader settingsLoader = new FXMLLoader(this.getClass().getResource("Settings.fxml"));
        Parent settingNode = settingsLoader.load();
        SettingsController settingsController = settingsLoader.getController();
        controller.getContainerVBox().getChildren().add(settingNode);
        //((GridPane)((ScrollPane)settingNode).getContent()).setPrefWidth(controller.getContainerVBox().getWidth());
        //(((ScrollPane)settingNode).getContent()).
        //VBox.setVgrow((ScrollPane)settingNode,Priority.ALWAYS);
        VBox.setVgrow(((GridPane)settingNode), Priority.ALWAYS);
        HBox.setHgrow(((GridPane)settingNode), Priority.ALWAYS);
        //((GridPane)((ScrollPane)settingNode).getContent()).setBackground(new Background(new BackgroundFill(Color.color(1,0,0),CornerRadii.EMPTY, Insets.EMPTY)));




        // Start le service

        /*SimulationService */simulationService = new SimulationService();
        AIManagerInterface aiManager = new AIManager(settingsController);
        AgentManagerInterface agentManager = new AgentManager(settingsController);
        SimulationInterface simulation = new Simulation(settingsController, simulationService, aiManager, agentManager);
        agentManager.setSimulation(simulation);
        agentManager.generateAgents();
        settingsController.setSimulationInterface(simulation);
        simulationService.setSimulation(simulation);
        simulationService.setSimSpeed(50);

        // draws the background, zones, agents and gen number when value updates
        simulationService.valueProperty().addListener((a, o,n)->{

            simCanvas.drawBackground(new int[]{120, 120, 120});

            for (ZoneShape zones:n.getZoneShapes()) {
                simCanvas.drawRelativeCircle(zones.getRelX(), zones.getRelY(), zones.getRadius(),zones.getColor());
            }

            for (AgentShape agentShape:n.getAgentShapes()) {
                simCanvas.drawRelativeCircle(agentShape.getRelX(), agentShape.getRelY(), agentShape.getRelSize(), agentShape.getColor());
            }
            simCanvas.drawGenNumber(n.getGenNumber());
            //simCanvas.drawRelativeCircle(0.5,0.5,0.2,new int[]{255,255,255});
        });

        simulationService.setOnFailed((e)->simulationService.getException().printStackTrace());
        simulationService.restart();



        // update clicked agent ai with listener on service
        simulationService.messageProperty().addListener((e)->{
            if(aigui!=null && currAI!=null)
                aigui.updateAI(currAI);
        });

        /*primaryStage.widthProperty().addListener((a,b,c)->{
            controller.getContainerVBox().setMinWidth((double)b-((Canvas)((BorderPane)canvasParent).getChildren().get(0)).getWidth());
            controller.getContainerVBox().setPrefWidth((double)b-((Canvas)((BorderPane)canvasParent).getChildren().get(0)).getWidth());
            controller.getContainerVBox().setMaxWidth((double)b-((Canvas)((BorderPane)canvasParent).getChildren().get(0)).getWidth());
            aigui.setWidth(controller.getContainerVBox().getPrefWidth());
            aigui.setHeight(controller.getContainerVBox().getPrefWidth()/3*2);
            aigui.updateAI();



            //((Canvas)((BorderPane)canvasParent).getChildren().get(0)).getWidth()
            //System.out.println(aigui.getWidth()+", "+controller.getContainerVBox().getPrefWidth()+", "+(primaryStage.getWidth()-((Canvas)((BorderPane)canvasParent).getChildren().get(0)).getWidth()));
            System.out.println((aigui.getAnchorPane().getPrefWidth()+((BorderPane)canvasParent).getWidth())+", "+primaryStage.getWidth());
        });*/
    }

    public static void main(String[] args) {
        launch();
    }

    /**
     * Debug code for moving and drawing an agent
     * @param scene : the scene the agent is in
     * @param simCanvas : the canvas to draw to
     * @param agents : the agents list to test
     */
    private void setKeyMouvement(Scene scene, CanvasInterface simCanvas, Agent... agents) {
        scene.setOnKeyPressed(e -> {
            simCanvas.drawBackground(new int[]{120, 120, 120});
            for (Agent agent : agents) {
                if (e.getCode() == KeyCode.D || e.getCode() == KeyCode.RIGHT) {
                    agent.setAngle(0);
                    agent.update();
                }
                else if (e.getCode() == KeyCode.A || e.getCode() == KeyCode.LEFT) {
                    agent.setAngle(PI);
                    agent.update();
                }
                else if (e.getCode() == KeyCode.S || e.getCode() == KeyCode.DOWN) {
                    agent.setAngle(PI / 2);
                    agent.update();
                }
                else if (e.getCode() == KeyCode.W || e.getCode() == KeyCode.UP) {
                    agent.setAngle(3 * PI / 2);
                    agent.update();
                }
                simCanvas.drawRelativeCircle(agent.getRelX(), agent.getRelY(), 0.01, agent.getColor());
            }
            e.consume();
        });




    }


}
