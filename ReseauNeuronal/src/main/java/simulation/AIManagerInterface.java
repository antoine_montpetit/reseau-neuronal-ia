package simulation;

import ai.AIInterface;

import java.util.ArrayList;

public interface AIManagerInterface {
    ArrayList<AIInterface> getAIs();
    // why tho
    void setAIs(ArrayList<AIInterface> ais);

    void generateRandomAIs();
    void generateUpdatedAIs();
}
