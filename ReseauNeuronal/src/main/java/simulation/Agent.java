package simulation;

import ai.AIInterface;
import mikera.vectorz.Vector;

import java.awt.geom.Point2D;


public class Agent implements AgentInterface {

    private double relY;
    private double relX;
    private double angle;
    private double speed;
    private AIInterface ai;
    private AgentManagerInterface agentManager;
    private double score;
    private double relSize;
    private int[] color;

    //init the agent as not being dead (alive)
    private boolean agentIsDead = false;
    private static final double BASE_SCORE = 0;
    private static final double MAX_SPEED = 1;
    private static final double MAX_SIZE = 1000;
    private static final double MIN_SIZE = 0;
    private static final double STARTING_POINT = MAX_SIZE/2;
    public Agent(double relY, double relX, double angle, double speed, AIInterface ai) {
        this();
        setRelX(relX);
        setAI(ai);
        setAngle(angle);
        setRelY(relY);
        setSpeed(speed);
    }

    public Agent(){
        setRelY(STARTING_POINT/MAX_SIZE); //now ok
        setRelX(STARTING_POINT/MAX_SIZE); //now ok
        setAI(null); //ok?
        setAngle(0); //ok
        setSpeed(MAX_SPEED); //ok
        score = BASE_SCORE;
        color = new int[]{255, 255, 255};
        relSize = 10/MAX_SIZE; // a proportion of the canvas width, so 1%
    }
    //kills the agent :(
    public void killAgent()
    {
        setAgentIsDead(true);
    }

    @Override
    public double getRelX() {
        return relX;
    }

    @Override
    public double getRelY() {
        return relY;
    }

    @Override
    public void setRelX(double relX) {
        if(relX > MAX_SIZE)
            this.relX = MAX_SIZE;
        else if (relX < MIN_SIZE)
            this.relX = MIN_SIZE;
        else
        this.relX = relX;
    }

    @Override
    public void setRelY(double relY) {
        if(relY > MAX_SIZE)
            this.relY = MAX_SIZE - 8;
        else if (relY < MIN_SIZE)
            this.relY = MIN_SIZE;
        else
            this.relY = relY;
    }

    @Override
    public double getAngle() {
        return angle;
    }

    @Override
    public void setAngle(double angle) {
        this.angle = angle;
    }

    @Override
    public double getSpeed() {
        return speed;
    }

    @Override
    public void setSpeed(double speed) {
        this.speed = speed;
    }

    @Override
    public AIInterface getAI() {
        return ai;
    }

    @Override
    public void setAI(AIInterface ai) {
        this.ai = ai;
    }

    @Override
    public double getScore() {
        return score;
    }

    @Override
    public void setAgentManager(AgentManagerInterface agentManager) {
        this.agentManager = agentManager;
    }

    public void setColor(int[] color){
        this.color = color;
    }
    public int[] getColor(){
        return color;
    }
    public boolean getAgentIsDead() {
        return agentIsDead;
    }

    public void setAgentIsDead(boolean agentIsDead) {
        this.agentIsDead = agentIsDead;
    }

    @Override
    public void update() {

        //todo ajouter l'update du score

        //Sending a Vector (Distance to pos zone, Direction to nearest pos zone, "radius" of pos zone, Distance to neg zone ...)

        //data for positive zone (if it exists)
        ZoneInterface nearestPositive = agentManager.getNearestPositiveZone(this);
        double posDist=0, posDirec=0, posRadius=0;
        if(nearestPositive!=null) {
            posDist = Point2D.distance(relX, relY, nearestPositive.getRelX(), nearestPositive.getRelY());
            posDirec = Math.atan2(nearestPositive.getRelY() - relY, nearestPositive.getRelX() - relX);
            posRadius = nearestPositive.getRadius(relX, relY);
        }
        //data for negative zone
        ZoneInterface nearestNegative = agentManager.getNearestNegativeZone(this);
        double negDist=0, negDirec=0, negRadius=0;
        if(nearestNegative!=null) {
            negDist = Point2D.distance(relX, relY, nearestNegative.getRelX(), nearestNegative.getRelY());
            negDirec = Math.atan2(nearestNegative.getRelY() - relY, nearestNegative.getRelX() - relX);
            negRadius = nearestNegative.getRadius(relX, relY);
        }
        //uses information of the agent relative to the canvas to use as inputs for
        Vector tempVector = Vector.of(posDist,posDirec,posRadius,negDist,negDirec,negRadius);
        Vector output = ai.compute(tempVector);
       //uses the outputs to set the speed and angle of the agent
        this.setSpeed(Math.min(Math.max(output.get(0),-2),2));
        this.setAngle(output.get(1));

        setRelX(Math.cos(angle)*(speed* MAX_SPEED)/MAX_SIZE + getRelX());
        setRelY(Math.sin(angle)*(speed* MAX_SPEED)/MAX_SIZE + getRelY());

    }

    @Override
    public double getRelSize() {
        return relSize;
    }
}
