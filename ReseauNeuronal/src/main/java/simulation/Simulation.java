package simulation;

import ai.AIInterface;
import controllers.SettingsController;
import gui.AgentShape;
import gui.SettingsInterface;
import gui.SimShape;
import gui.ZoneShape;

import java.util.ArrayList;

public class Simulation implements SimulationInterface {

    private boolean pause;
    private boolean isReset;
    private double time;
    private double speed;
    private double nbAgent;
    private double mutationLevel;
    private boolean isGenDone;
    private int genNumber;
    private AgentManagerInterface agentManager;
    private AIManagerInterface aiManager;
    private ArrayList<ZoneInterface> zones;
    private gui.SettingsInterface settings;
    private static final double width = 100;
    private static final double height = 100;
    private SimulationService simulationService;


    public Simulation(SettingsInterface settings, SimulationService simulationService, AIManagerInterface aiManager, AgentManagerInterface agentManager){
        this.settings = settings;
        this.simulationService = simulationService;
        this.aiManager = aiManager;
        this.agentManager = agentManager;
        updateSimulationSpeed(settings.getGenSpeed());
        updateCurrentSettings();
        zones = new ArrayList<ZoneInterface>();
        isGenDone = false;


    }

    public AgentManagerInterface getAgentManager() {
        return agentManager;
    }

    @Override
    public void togglePause() {
        pause = !pause;
    }

    @Override
    public void reset() {
        isReset = true;
    }

    @Override
    public void updateSimulationSpeed(double speed) {
        this.speed = speed;
        simulationService.setSimSpeed((int)speed);
    }



    @Override
    public void updateCurrentSettings() {
    nbAgent = settings.getAgentAmount();
    speed = settings.getGenSpeed();
    time = settings.getTimePerGen();
    mutationLevel = settings.getMutationLevel();
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public ArrayList<ZoneInterface> getZones() {
        return zones;
    }

    @Override
    public ArrayList<Double> generateInputs(int x, int y) {
        return null;
    }

    @Override
    public void init() {
        isReset = false;
        updateCurrentSettings();
        agentManager.generateAgents();
        aiManager.generateRandomAIs();
        agentManager.pairAIs(aiManager);

        for(int i=0;i<10;i++){
            ZoneInterface curr = new ZoneStub();
            curr.setRelX(Math.random());
            curr.setRelY(Math.random());
            curr.setRadius(Math.random()/5);
            curr.setScore(Math.random()*2-1);
            addZone(curr);
        }
    }

    @Override
    public void update() {
        for (AgentInterface agent:agentManager.getAgents()) {
            agent.update();
        }
    }

    @Override
    public boolean isReset() {
        return isReset;
    }

    @Override
    public boolean isPaused() {
        return pause;
    }

    @Override
    public void setIsGenDone(boolean isGenDone) {
        this.isGenDone = isGenDone;
    }

    @Override
    public boolean getIsGenDone() {
        return isGenDone;
    }

    @Override
    public double getTime() {
        return time;
    }

    @Override
    public double getSpeed() {
        return speed;
    }

    @Override
    public SimShape getGraphics() {
        ArrayList<AgentShape> agentShapes = new ArrayList<>();
        ArrayList<ZoneShape> zoneShapes = new ArrayList<>();

        for (AgentInterface agent:agentManager.getAgents()) {
            agentShapes.add(new AgentShape(agent.getRelX(),agent.getRelY(),agent.getRelSize(),new int[]{255,255,255}));
        }
        for (ZoneInterface zone:zones) {
            zoneShapes.add(new ZoneShape(zone.getRelX(),zone.getRelY(),zone.getRadius(0,0), zone.getScore()));
        }



        return new SimShape(agentShapes, zoneShapes, getGenNumber());
    }

    @Override
    public void nextGen() {
        isGenDone = false;
        //Todo sprint 3
    }

    @Override
    public void addZone(ZoneInterface zone) {
        this.zones.add(zone);
    }

    @Override
    public void setAgentManager(AgentManagerInterface agentManager) {
        this.agentManager = agentManager;
    }

    public int getGenNumber(){return genNumber;}
    public void setGenNumber(int genNumber){this.genNumber = genNumber;}
    // dubious utility
    public void setAgents(ArrayList<AgentInterface> agents){
        this.agentManager.setAgents(agents);
    }
    // also dubious utility
    public ArrayList<AgentInterface> getAgents(){
        return agentManager.getAgents();
    }
}
