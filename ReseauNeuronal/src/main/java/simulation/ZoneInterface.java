package simulation;

public interface ZoneInterface {
    double getRelX();
    double getRelY();
    void setRelX(double x);
    void setRelY(double y);
    double getScore();
    void setScore(double score);

    // get physical parameters
    double getDistToEdge(double x, double y);
    double getEdgeToCenter(double x, double y);
    double getDirection(double x, double y);

    double getRadius(double x, double y);
    void setRadius(double rad);

}
