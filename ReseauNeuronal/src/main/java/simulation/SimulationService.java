package simulation;

import ai.AIInterface;
import gui.AIGUI;
import gui.SimShape;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.util.ArrayList;

public class SimulationService extends Service<SimShape> {
    // 100 tps
    public static final int BASE_DELAY = 100;
    private ArrayList<Double> settings;
    private SimulationInterface simulation;
    private double simSpeed;
    private int progress;

    public void updateSettings(ArrayList<Double> settings){
        this.settings=settings;
    }
    public void setSimSpeed(int simSpeed){
        this.simSpeed= simSpeed;
    }
    public void setSimulation(SimulationInterface simulation){
        this.simulation=simulation;
    }
    public SimulationInterface getSimulation(){return(this.simulation);};





    @Override
    protected Task<SimShape> createTask() {
        return(new Task<SimShape>() {
            @Override
            protected SimShape call() throws Exception {
                // do gens until task is restarted
                while(!isCancelled()){
                    // init values/settings and new agents/ais
                    simulation.init();
                    progress = 0;
                    // do this on loop until reset
                    while(!simulation.isReset()){
                        while(!simulation.getIsGenDone()) {
                            // update ai graphics to current ai
                            updateMessage("hi"+Math.random());

                            // if not paused
                            if (!simulation.isPaused()) {
                                // update agents and stuff
                                simulation.update();
                                updateValue(simulation.getGraphics());

                            }
                            Thread.sleep((long) (1.0 / ((double) simSpeed) * ((double) BASE_DELAY)));
                            progress += (long) (1.0 / ((double) simSpeed) * ((double) BASE_DELAY));
                            if (progress >= simulation.getTime()){
                                simulation.setIsGenDone(true);
                            }
                        }
                        simulation.setGenNumber(simulation.getGenNumber()+1);
                        progress = 0;
                        simulation.nextGen();
                    }

                }
                return(null);
            }
        });
    }
}
