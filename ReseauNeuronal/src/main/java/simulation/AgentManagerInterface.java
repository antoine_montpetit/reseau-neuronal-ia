package simulation;

import java.util.ArrayList;

public interface AgentManagerInterface {
    ArrayList<AgentInterface> getAgents();
    // for testing
    void setAgents(ArrayList<AgentInterface> agents);

    // Setup update
    void pairAIs(AIManagerInterface aiManager);
    void generateAgents();

    void setSimulation(SimulationInterface simulation);

    // Get Data
    ZoneInterface getNearestPositiveZone(AgentInterface agent);
    ZoneInterface getNearestNegativeZone(AgentInterface agent);

}
