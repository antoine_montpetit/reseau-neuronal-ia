package simulation;

import ai.AIInterface;

public interface AgentInterface {
    // attributs: score,x,y,angle,speed,ai,...
    double getRelX();
    double getRelY();
    void setRelX(double relX);
    void setRelY(double relY);
    double getAngle();
    void setAngle(double angle);
    double getSpeed();
    void setSpeed(double speed);
    AIInterface getAI();
    void setAI(AIInterface ai);
    double getScore();
    void setAgentManager(AgentManagerInterface agentManager);
    void update();
    double getRelSize();
}
