package simulation;

import ai.AI;
import ai.AIInterface;
import gui.SettingsInterface;

import java.util.ArrayList;

public class AgentManager implements AgentManagerInterface{

    private SimulationInterface simulation;
    private SettingsInterface settings;
    private ArrayList<AgentInterface> agents;

    public AgentManager(SettingsInterface settings){
        this.agents = new ArrayList<AgentInterface>();
        this.settings = settings;
    }

    public void setSimulation(SimulationInterface simulation){
        this.simulation=simulation;
    }

    @Override
    public ArrayList<AgentInterface> getAgents() {
        return agents;
    }

    @Override
    public void setAgents(ArrayList<AgentInterface> agents) {
        this.agents = agents;
    }

    @Override
    public void pairAIs(AIManagerInterface aiManager) {

        // If the array don't match QUIT
        if(aiManager.getAIs().size() != agents.size())
            throw new ParingException("The size of the array of agent wasn't equal to the size of the array of agent");

        // Associate
        for (int i = 0; i < aiManager.getAIs().size(); i++) {
            agents.get(i).setAI(aiManager.getAIs().get(i));
        }

    }

    @Override
    public void generateAgents() {
        int numOfAgent = settings.getAgentAmount();
        agents.clear();
        for (int i = 0; i < numOfAgent; i++) {
            Agent currAgent = new Agent();
            currAgent.setAgentManager(this);
            agents.add(currAgent);
        }
    }

    @Override
    public ZoneInterface getNearestPositiveZone(AgentInterface agent) {

        double agentRelX = agent.getRelX();
        double agentRelY = agent.getRelY();

        double minDistance = Double.POSITIVE_INFINITY;
        ZoneInterface minZone = null;

        // may happen if there exists no zone
        if(simulation.getZones()==null)
            return null;

        for (ZoneInterface zone: simulation.getZones()) {

            if(zone.getScore() <= 0)
                continue;

            double currDistance = Math.pow(agentRelX - zone.getRelX(),2) + Math.pow(agentRelY - zone.getRelY(),2);

            if(currDistance < minDistance){
                minDistance = currDistance;
                minZone = zone;
            }

        }

        return minZone;
    }

    @Override
    public ZoneInterface getNearestNegativeZone(AgentInterface agent) {

        double agentRelX = agent.getRelX();
        double agentRelY = agent.getRelY();

        double minDistance = Double.POSITIVE_INFINITY;
        ZoneInterface minZone = null;

        // may happen if there exists no zone
        if(simulation.getZones()==null)
            return null;

        for (ZoneInterface zone: simulation.getZones()) {

            if(zone.getScore() >= 0)
                continue;

            double currDistance = Math.pow(agentRelX - zone.getRelX(),2) + Math.pow(agentRelY - zone.getRelY(),2);

            if(currDistance < minDistance){
                minDistance = currDistance;
                minZone = zone;
            }

        }

        return minZone;
    }


}

class ParingException extends IndexOutOfBoundsException{
    public ParingException() {
        super();
    }

    public ParingException(String s) {
        super(s);
    }
}
