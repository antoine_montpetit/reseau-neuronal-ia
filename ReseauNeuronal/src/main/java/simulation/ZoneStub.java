package simulation;

public class ZoneStub implements ZoneInterface{

    double relX;
    double relY;
    double score;
    double radius;

    @Override
    public double getRelX() {
        return relX;
    }

    @Override
    public double getRelY() {
        return relY;
    }

    @Override
    public void setRelX(double x) {
        this.relX=x;
    }

    @Override
    public void setRelY(double y) {
        this.relY=y;
    }


    @Override
    public double getScore() {
        return score;
    }

    @Override
    public void setScore(double score) {
        this.score=score;
    }

    @Override
    public double getDistToEdge(double x, double y) {
        return 0;
    }

    @Override
    public double getEdgeToCenter(double x, double y) {
        return 0;
    }

    @Override
    public double getDirection(double x, double y) {
        return 0;
    }

    @Override
    public double getRadius(double x, double y) {
        return radius;
    }

    @Override
    public void setRadius(double rad) {
        this.radius=rad;
    }
}
