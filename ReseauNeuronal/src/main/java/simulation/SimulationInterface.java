package simulation;

import gui.SimShape;

import java.util.ArrayList;

public interface SimulationInterface {
    // settings updated real-time on simulation
    void togglePause();
    void reset();

    void updateSimulationSpeed(double speed);
    void updateCurrentSettings();
    double getWidth();
    double getHeight();
    void setGenNumber(int genNumber);
    int getGenNumber();
    ArrayList<ZoneInterface> getZones();
    ArrayList<Double> generateInputs(int x, int y);
    void init();
    void update();
    boolean isReset();
    boolean isPaused();
    void setIsGenDone(boolean isGenDone);
    boolean getIsGenDone();
    double getTime();
    double getSpeed();
    SimShape getGraphics();
    void nextGen();
    void addZone(ZoneInterface zone);

    void setAgentManager(AgentManagerInterface agentManager);
    AgentManagerInterface getAgentManager();
}
