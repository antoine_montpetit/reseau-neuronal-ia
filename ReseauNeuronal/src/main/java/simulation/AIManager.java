package simulation;

import ai.AI;
import ai.AIInterface;
import gui.SettingsInterface;

import java.util.ArrayList;

public class AIManager implements AIManagerInterface{

    public SettingsInterface getSettings() {
        return settings;
    }

    private SettingsInterface settings;
    private ArrayList<AIInterface> ais;

    public AIManager(SettingsInterface settings){
        this.ais = new ArrayList<AIInterface>();
        this.settings = settings;
    }

    @Override
    public ArrayList<AIInterface> getAIs() {
        return ais;
    }

    @Override
    public void setAIs(ArrayList<AIInterface> ais) {
        this.ais = ais;
    }

    @Override
    public void generateRandomAIs() {
        int numOfAgent = settings.getAgentAmount();
        for (int i = 0; i < numOfAgent; i++) {
            ais.add(new AI(this));
        }
    }

    @Override
    public void generateUpdatedAIs() {
        // TODO : SPRINT 3
    }
}
